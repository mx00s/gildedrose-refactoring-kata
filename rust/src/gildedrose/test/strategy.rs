use crate::gildedrose::{Item, GildedRose};
use super::{AGED_BRIE, BACKSTAGE_PASS, SULFURAS, LEGENDARY_QUALITY, NORMAL_MAX_QUALITY};
use proptest::prelude::*;
use std::ops::RangeInclusive;
use crate::gildedrose::test::MAX_DAYS;

/// Strategy for `sell_in` days for `Items`.
const SELL_IN_RANGE: RangeInclusive<i32> = 0..=MAX_DAYS as i32;

/// Strategy for non-exceptional `Item` quality ("never negative" and "never more than 50").
const NORMAL_QUALITY_RANGE: RangeInclusive<i32> = 0..=NORMAL_MAX_QUALITY;

/// Strategy for item name which shouldn't be confused with any of the special names.
const PLAIN_NAME: &'static str = "[a-z]{3}";

prop_compose! {
    pub fn sulfuras()(sell_in in SELL_IN_RANGE) -> Item {
        Item::new(SULFURAS.to_string(), sell_in, LEGENDARY_QUALITY)
    }
}

pub fn legendary() -> impl Strategy<Value = Item> {
    sulfuras()  // only known legendary item
}

prop_compose! {
    pub fn regular_item(name: String)(
        sell_in in SELL_IN_RANGE,
        quality in NORMAL_QUALITY_RANGE,
    ) -> Item {
        Item::new(name.clone(), sell_in, quality)
    }
}

pub fn aged_brie() -> impl Strategy<Value = Item> {
    regular_item(AGED_BRIE.to_string())
}

pub fn backstage_pass() -> impl Strategy<Value = Item> {
    regular_item(BACKSTAGE_PASS.to_string())
}

fn item() -> impl Strategy<Value = Item> {
    prop_oneof![
        backstage_pass(),
        legendary(),
        (PLAIN_NAME, SELL_IN_RANGE, NORMAL_QUALITY_RANGE).prop_map(|(n, s, q)|
            Item::new(n, s, q)
        )
    ]
}

prop_compose! {
    pub fn gilded_rose(max_items: usize)(
        items in prop::collection::vec(item(), 1..=max_items)
    ) -> GildedRose {
        GildedRose::new(items)
    }
}

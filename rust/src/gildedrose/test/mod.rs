use super::GildedRose;
use proptest::prelude::*;
use std::{collections::HashMap, ops::RangeInclusive};
use crate::gildedrose::{Item, AGED_BRIE, BACKSTAGE_PASS, SULFURAS, NORMAL_MAX_QUALITY,
                        LEGENDARY_QUALITY};

mod strategy;

const MAX_ITEMS: usize = 5;
const MAX_DAYS: usize = 100;
const DAY_RANGE: RangeInclusive<usize> = 1..=MAX_DAYS;

fn item_names(vendor: &GildedRose) -> Vec<String> {
    vendor
        .items
        .iter()
        .map(|i| i.name.clone())
        .collect::<Vec<_>>()
}

proptest! {
    #[test]
    fn item_names_do_not_change_as_they_age(
        mut vendor in strategy::gilded_rose(MAX_ITEMS),
        days_to_age in DAY_RANGE,
    ) {
        let young_names = item_names(&vendor);
        for _ in 0..days_to_age {
            vendor.update_quality();
        }
        let aged_names = item_names(&vendor);

        prop_assert_eq!(aged_names, young_names);
    }
}

proptest! {
    /// The goal of an oracle test is to implement a cleaner version which behaves the same as
    /// the original.
    ///
    /// Bugs in the update quality behavior will only be considered after parity is reached with
    /// the old behavior, and the code is cleaner. At that point bugs can be fixed more
    /// confidence that regressions aren't also getting introduced.
    #[test]
    fn update_quality_test_oracle_to_ensure_parity(
        mut vendor in strategy::gilded_rose(MAX_ITEMS),
        days_to_age in DAY_RANGE,
    ) {
        let mut vendor_copy = vendor.clone();
        for _ in 0..days_to_age {
            vendor.update_quality();
            vendor_copy.update_quality_original();
        }
        prop_assert_eq!(vendor, vendor_copy);
    }
}

fn is_item_quality_valid(item: &Item) -> bool {
    let non_negative = item.quality >= 0;
    let within_upper_bound = if item.quality == LEGENDARY_QUALITY {
        &item.name == SULFURAS
    } else {
        item.quality <= 50
    };

    non_negative && within_upper_bound
}

proptest! {
    #[test]
    fn item_quality_is_between_0_and_50_unless_legendary80(
        mut vendor in strategy::gilded_rose(MAX_ITEMS),
        days_to_age in DAY_RANGE,
    ) {
        assert!(vendor.items.iter().all(is_item_quality_valid));
        for _ in 0..days_to_age {
            vendor.update_quality();
            assert!(vendor.items.iter().all(is_item_quality_valid));
        }
    }
}

proptest! {
    #[test]
    fn after_sell_by_date_quality_degrades_twice_as_fast(
        mut vendor in strategy::gilded_rose(MAX_ITEMS),
        days_to_age in DAY_RANGE,
    ) {
        let mut prior_quality_of_expired_items: HashMap<usize, i32> = HashMap::new();

        for _ in 0..days_to_age {
            vendor.update_quality();
            for (index, item) in vendor.items.iter().enumerate() {
                if item.sell_in == 0 {
                    prior_quality_of_expired_items
                        .entry(index)
                        .and_modify(|e| {
                            let observed_rate: i32 = item.quality - *e;
                            let normal_rate: i32 = if item.quality == LEGENDARY_QUALITY {
                                0   // items of legendary quality don't degrade
                            } else {
                                1
                            };

                            assert_eq!(observed_rate, normal_rate * 2);

                            *e = item.quality;
                        })
                        .or_insert(item.quality);
                }
            }
        }
    }
}

proptest! {
    #[test]
    fn legendary_items_never_have_to_be_sold_or_decrease_in_quality(
        item in strategy::legendary(),
        days_to_age in DAY_RANGE,
    ) {
        prop_assert_eq!(&item.quality, &LEGENDARY_QUALITY);
        let original_sell_in = item.sell_in;

        let mut vendor = GildedRose::new(vec![item]);

        for _ in 0..days_to_age {
            vendor.update_quality();

            prop_assert_eq!(vendor.items[0].sell_in, original_sell_in);
            prop_assert_eq!(vendor.items[0].quality, LEGENDARY_QUALITY);
        }
    }
}

proptest! {
    #[test]
    fn aged_brie_increases_in_quality_as_it_ages_until_reaching_50(
        item in strategy::aged_brie(),
        days_to_age in DAY_RANGE,
    ) {
        let mut prior_quality = item.quality;

        let mut vendor = GildedRose::new(vec![item]);

        for _ in 0..days_to_age {
            vendor.update_quality();
            let curr_quality = &vendor.items[0].quality;

            let increased = *curr_quality > prior_quality;
            let maxed_out = *curr_quality == NORMAL_MAX_QUALITY;

            assert!(increased || maxed_out,
                    format!("prior: {}, current: {}",
                        prior_quality,
                        *curr_quality));
            prior_quality = *curr_quality;
        }

    }
}

proptest! {
    #[test]
    fn backstage_pass_items_increase_by_2_in_quality_when_6_to_10_days_left(
        mut item in strategy::backstage_pass(),
        sell_in in 6..=10,
    ) {
        item.sell_in = sell_in;
        let original_quality = item.quality;

        let mut vendor = GildedRose::new(vec![item]);
        vendor.update_quality();

        prop_assert_eq!(vendor.items[0].quality, (original_quality + 2).min(NORMAL_MAX_QUALITY));
    }
}

proptest! {
    #[test]
    fn backstage_pass_items_increase_by_3_in_quality_when_1_to_5_days_left(
        mut item in strategy::backstage_pass(),
        sell_in in 1..=5,
    ) {
        item.sell_in = sell_in;
        let original_quality = item.quality;

        let mut vendor = GildedRose::new(vec![item]);
        vendor.update_quality();

        prop_assert_eq!(vendor.items[0].quality, (original_quality + 3).min(NORMAL_MAX_QUALITY));
    }
}

// TODO: "'Conjured' items degrade in Quality twice as fast as normal items" (requires code update)

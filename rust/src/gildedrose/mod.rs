use std::string;
use std::vec;

const AGED_BRIE: &'static str = "Aged Brie";
const BACKSTAGE_PASS: &'static str = "Backstage passes to a TAFKAL80ETC concert";
const SULFURAS: &'static str = "Sulfuras, Hand of Ragnaros";

const NORMAL_MAX_QUALITY: i32 = 50;

/// "'Sulfuras' is a legendary item and as such its Quality is 80 and it never alters"
const LEGENDARY_QUALITY: i32 = 80;


#[derive(Debug, Clone, PartialEq)]
pub struct Item {
    pub name: string::String,
    pub sell_in: i32,
    pub quality: i32
}

impl Item {
    pub fn new(name: String, sell_in: i32, quality: i32) -> Item {
        Item {name: name, sell_in: sell_in, quality: quality}
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct GildedRose {
    pub items: vec::Vec<Item>
}

impl GildedRose {
    pub fn new(items: vec::Vec<Item>) -> GildedRose {
        GildedRose {items: items}
    }

    pub fn update_quality_original(&mut self) {
        for item in &mut self.items {
            if item.name != "Aged Brie" && item.name != "Backstage passes to a TAFKAL80ETC concert" {
                if item.quality > 0 {
                    if item.name != "Sulfuras, Hand of Ragnaros" {
                        item.quality = item.quality - 1;
                    }
                }
            } else {
                if item.quality < 50
                {
                    item.quality = item.quality + 1;

                    if item.name == "Backstage passes to a TAFKAL80ETC concert" {
                        if item.sell_in < 11 {
                            if item.quality < 50 {
                                item.quality = item.quality + 1;
                            }
                        }

                        if item.sell_in < 6 {
                            if item.quality < 50 {
                                item.quality = item.quality + 1;
                            }
                        }
                    }
                }
            }

            if item.name != "Sulfuras, Hand of Ragnaros" {
                item.sell_in = item.sell_in - 1;
            }

            if item.sell_in < 0 {
                if item.name != "Aged Brie" {
                    if item.name != "Backstage passes to a TAFKAL80ETC concert" {
                        if item.quality > 0 {
                            if item.name != "Sulfuras, Hand of Ragnaros" {
                                item.quality = item.quality - 1;
                            }
                        }
                    } else {
                        item.quality = item.quality - item.quality;
                    }
                } else {
                    if item.quality < 50 {
                        item.quality = item.quality + 1;
                    }
                }
            }
        }
    }

    pub fn update_quality(&mut self) {
        for item in &mut self.items {
            let adjust_quality = |item: &mut Item, amount: i32|
                item.quality = (item.quality + amount)
                    .min(NORMAL_MAX_QUALITY)
                    .max(0);

            let appreciate_backstage = |item: &mut Item| {
                let amount = match item.sell_in {
                    days if days > 10 => 1,
                    days if days > 5 => 2,
                    _days => 3,
                };
                adjust_quality(item, amount);
            };

            let update_quality = |item: &mut Item| {
                match &item.name[..] {
                    AGED_BRIE => adjust_quality(item, 1),
                    BACKSTAGE_PASS => appreciate_backstage(item),
                    SULFURAS => {},
                    _ => adjust_quality(item, -1),
                };
            };

            let update_expiration = |item: &mut Item|
                match &item.name[..] {
                    SULFURAS => {},
                    _ => item.sell_in -= 1,
                };

            let handle_expiration = |item: &mut Item|
                if item.sell_in < 0 {
                    match &item.name[..] {
                        AGED_BRIE => adjust_quality(item, 1),
                        BACKSTAGE_PASS => item.quality = 0,
                        SULFURAS => {},
                        _ => adjust_quality(item, -1),
                    }
                };

            update_quality(item);
            update_expiration(item);
            handle_expiration(item);
        }
    }
}

#[cfg(test)]
mod test;

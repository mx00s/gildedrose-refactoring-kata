#!/usr/bin/env bash

set -e
set -x

echo "Starting in directory: $(pwd)"
pushd ./rust
cargo build
popd

#!/usr/bin/env bash

set -e

REPO_ROOT="$(pwd)"

function rust_test_suite {
    echo "Running rust test suite ..."
    cargo test
}

function thirty_days_texttest {
    local actual_stdout="/tmp/stdout"
    local expected_stdout="${REPO_ROOT}/texttests/ThirtyDays/stdout.gr"

    echo "Running thirty days texttest ..."

    cargo run > "$actual_stdout"

    diff --color --strip-trailing-cr \
	 "$expected_stdout" \
	 "$actual_stdout"
    echo "PASS: No differences found!"
}

pushd "${REPO_ROOT}/rust"
rust_test_suite
thirty_days_texttest
popd
